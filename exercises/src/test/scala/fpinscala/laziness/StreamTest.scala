package fpinscala.laziness

import fpinscala.laziness.Stream.{constant, cons}
import org.specs2.mutable.Specification

class StreamTest extends Specification {
  "Streams" >> {
    val stream = Stream(1, 2, 3, 4)

    "can be converted to Lists" >> {
      stream.toList must_== List(1, 2, 3, 4)
    }

    "can be taken from" >> {
      stream.take(2).toList must_== List(1, 2)
    }

    "can drop elements" >> {
      stream.drop(2).toList must_== List(3, 4)
    }

    "can take while" >> {
      stream.takeWhile(_ < 3).toList must_== List(1, 2)
      stream.takeWhile_fr(_ < 3).toList must_== List(1, 2)
    }

    "check all match but terminates early if they don't" >> {
      stream.forAll(_ < 5) must_== true
      stream.forAll(_ > 2) must_== false
      cons(1, cons(???, Stream.empty)).forAll(_ > 1) must_== false
    }

    "head option" >> {
      stream.headOption must_== Some(1)
      Stream.empty.headOption must_== None
    }

    "can map the values" >> {
      Stream(1, 2, 3, 4).map(_.toString).toList must_== List("1", "2", "3", "4")
      cons(1, cons(???, Stream.empty)).map(_.toString).headOption must_== Some("1")
    }

    "append two streams" >> {
      Stream(1, 2).append(Stream(3, 4)).toList must_== List(1, 2, 3, 4)
    }

    "create an infinite stream of values" >> {
      constant(1).take(4).toList must_== List(1, 1, 1, 1)
    }

    "generate an incrementing stream" >> {
      Stream.from(5).take(3).toList must_== List(5, 6, 7)
    }

    "fibonacci" >> {
      Stream.fibs.take(5).toList must_== List(0, 1, 1, 2, 3)
    }
  }
}
