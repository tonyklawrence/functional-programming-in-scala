package fpinscala.datastructure

import fpinscala.datastructures.Tree._
import fpinscala.datastructures.{Tree, Branch, Leaf}

import org.specs2.mutable.Specification

class TreeTest extends Specification {
  "A tree should" >> {
    val tree = Branch(Branch(Leaf(7), Leaf(9)), Leaf(4))

    "have a size" >> {
      Tree.size(tree) must_== 5
    }

    "can find the maximum leaf" >> {
      maximum(tree) must_== 9
    }

    "can find the deepest path" >> {
      Tree.depth(tree) must_== 2
    }

    "be mappable" >> {
      Tree.map(tree)(_ * 2) must_== Branch(Branch(Leaf(14), Leaf(18)), Leaf(8))
    }
  }
}