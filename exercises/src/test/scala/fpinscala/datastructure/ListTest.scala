package fpinscala.datastructure

import org.specs2.mutable.Specification
import fpinscala.datastructures._

class ListTest extends Specification {
  import List._

  "A List should" >> {
    val abc = List("A", "B", "C")

    "return it's tail" >> {
      tail(Nil)  must throwA[UnsupportedOperationException]
      tail(abc) must_== List("B", "C")
    }

    "be able to replace it's head" >> {
      setHead(Nil,  "Z") must throwA[UnsupportedOperationException]
      setHead(abc, "Z") must_== List("Z", "B", "C")
    }

    "drop entries from the front" >> {
      drop(Nil,  3) must_== Nil
      drop(abc, 0) must_== abc
      drop(abc, 1) must_== List("B", "C")
      drop(abc, 2) must_== List("C")
      drop(abc, 3) must_== Nil
      drop(abc, 4) must_== Nil
    }

    "drop entry while predicate holds true" >> {
      dropWhile(abc)(_ != "B") must_== List("B", "C")
      dropWhile(abc)(_ != "C") must_== List("C")
    }

    "return all but the last element" >> {
      init(Nil) must throwA[UnsupportedOperationException]
      init(abc) must_== List("A", "B")
    }

    "know how long it is" >> {
      List.length(Nil) must_== 0
      List.length(abc) must_== 3
    }

    "fold left" >> {
      foldLeft(abc, "")(_ + _) must_== "ABC"
    }

    "use foldleft for calculations" >> {
      sum_fl(List(4, 2, 8)) must_== 14
      product_fl(List(3.0, 2.0)) must_== 6
      length_fl(abc) must_== 3
    }

    "reverse the abc using fold" >> {
      reverse(abc) must_== List("C", "B", "A")
    }

    "append using folds" >> {
      append_fl(abc, List("D")) must_== List("A", "B", "C", "D")
    }

    "transforms the abc" >> {
      val numbers = List(1, 2, 3)
      List.map(numbers)(_ + 1) must_== List(2, 3, 4)
    }

    "turns a abc of doubles into strings" >> {
      val doubles = List(1.0, 2.0, 3.0)
      List.map(doubles)(_.toString) must_== List("1.0", "2.0", "3.0")
    }

    "filter out odd numbers" >> {
      val numbers = List(1, 2, 3, 4)
      List.filter(numbers)(_ % 2 == 0) must_== List(2, 4)
    }

    "flatMap" >> {
      val numbers = List(1, 2, 3)
      flatMap(numbers)(i ⇒ List(i,i)) must_== List(1,1,2,2,3,3)
    }

    "filter using flatmap" >> {
      val numbers = List(1, 2, 3, 4)
      filter_fm(numbers)(_ % 2 == 0) must_== List(2, 4)
    }

    "zip with" >> {
      zipWith(List(1,2,3), List(4,5,6))(_ + _) must_== List(5,7,9)
    }
  }
}