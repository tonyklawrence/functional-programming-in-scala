package fpinscala.errorhandling

import org.specs2.mutable.Specification

class EitherTest extends Specification {
  "An Either should" >> {
    "be a right biased map" >> {
      Right("foo").map(_.toUpperCase) must_== Right("FOO")
      Left("foo").map(_.getClass) must_== Left("foo")
    }

    "be right biased flat mapper" >> {
      Right("foo").flatMap(x ⇒ Right(x.toUpperCase)) must_== Right("FOO")
      Left("foo").flatMap(x ⇒ x) must_== Left("foo")
    }

    "allow alternatives" >> {
      Right("foo").orElse(Right("bar")) must_== Right("foo")
      Left("foo").orElse(Right("bar")) must_== Right("bar")
    }

    "be liftable" >> {
      Right("foo").map2(Right("bar"))(_ + _) must_== Right("foobar")
      Right("foo").map2(Left("bar"))(_ + _) must_== Left("bar")
      Left("foo").map2(Right("bar"))((_,_) ⇒ "far") must_== Left("foo")
      Left("foo").map2(Left("bar"))((_,_) ⇒ "far") must_== Left("foo")
    }

    import Either._
    "be traversable" >> {
      traverse(List(Right("foo"), Right("bar")))(x ⇒ x) must_== Right(List("foo", "bar"))
    }
  }
}