package fpinscala.errorhandling

import org.specs2.mutable.Specification

class OptionTest extends Specification {
  "Options should" >> {
    "be mappable if they have a value" >> {
      None.map(_.toString) must_== None
      Some(5).map(_.toString) must_== Some("5")
    }

    "allow us to supply an alternative value" >> {
      None.getOrElse("other") must_== "other"
      Some("thing").getOrElse("other") must_== "thing"
    }

    "flatten the mapped values" >> {
      None.flatMap(_ ⇒ None) must_== None
      None.flatMap(_ ⇒ Some("thing")) must_== None
      Some("thing").flatMap(_ ⇒ None) must_== None
      Some("thing").flatMap(_ ⇒ Some("other")) must_== Some("other")
    }

    "supply an alternative option" >> {
      None.orElse(Some("thing")) must_== Some("thing")
      Some("thing").orElse(None) must_== Some("thing")
    }

    "filter out" >> {
      None.filter(_ ⇒ false) must_== None
      None.filter(_ ⇒ true) must_== None
      Some("thing").filter(_ ⇒ false) must_== None
      Some("thing").filter(_ ⇒ true) must_== Some("thing")
    }
  }

  import Option._
  "Options are great because" >> {
    "we can lift to them" >> {
      map2(Some(1), Some(2))(_ + _) must_== Some(3)
    }

    "we can map over them" >> {
      val some = Some(1) :: Some(2) :: Some(3) :: Nil
      sequence(some) must_== Some(List(1, 2, 3))
      sequence(None :: some) must_== None
    }

    "we can traverse them" >> {
      traverse(List(1, 2, 3))(n ⇒ Some(n)) must_== Some(List(1, 2, 3))
    }
  }
}
