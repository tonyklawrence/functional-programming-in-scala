package fpinscala.state

import fpinscala.state.RNG._
import org.specs2.mutable.Specification

class StateTest extends Specification {
  "Chapter 6 - Purely functional state" >> {
    val rand = Simple(-1234L)

    "Exercise 6.1" >> {
      "Value must be non negative" >> {
        nonNegativeInt(rand)._1 must beGreaterThanOrEqualTo (0)
      }
    }

    "Exercise 6.2" >> {
      "Value should be between 0 and 1, not including 1" >> {
        double(rand)._1 must beGreaterThanOrEqualTo(0D) and beLessThan(1D)
      }
    }

    "Exercise 6.3" >> {
      "Generate an (Int, Double) pair" >> {
        val ((int, double), _) = intDouble(rand)
        int must beEqualTo(-474780143)
        double must beEqualTo(0.02651629038155079)
      }

      "Generate a (Double, Int) pair" >> {
        val ((double, int), _) = doubleInt(rand)
        double must beEqualTo(0.22108673211187124)
        int must beEqualTo(-56943301)
      }

      "Generate a (Double, Double, Double) 3-tuple" >> {
        val ((d1, d2, d3), _) = double3(rand)
        d1 must beEqualTo(0.22108673211187124)
        d2 must beEqualTo(0.02651629038155079)
        d3 must beEqualTo(0.5188320158049464)
      }
    }

    "Exercise 6.4" >> {
      "Generate a list of random integers" >> {
        val (is, _) = ints(5)(rand)
        is must beEqualTo(List(-474780143, -56943301, 1114183270, -1170997278, 479405772))
      }
    }

    "Exercise 6.5" >> {
      "Implement double using map" >> {
        val (double, _) = double_with_map(rand)
        double must beEqualTo(0.22108673211187124)
      }
    }

    "Exercise 6.6" >> {
      "Can combine 2 state functions" >> {
        val ((d, i), _) = map2(double, int)((_, _))(rand)
        d must beEqualTo(0.22108673211187124)
        i must beEqualTo(-56943301)
      }
    }
  }
}