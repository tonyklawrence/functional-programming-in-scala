package fpinscala.errorhandling


import scala.{Option => _, Some => _, Either => _} // hide std library `Option`, `Some` and `Either`, since we are writing our own in this chapter

sealed trait Option[+A] {
  def map[B](f: A => B): Option[B] = this match {
    case None ⇒ None
    case Some(n) ⇒ Some(f(n))
  }

  def getOrElse[B>:A](default: => B): B = this match {
    case None ⇒ default
    case Some(n) ⇒ n
  }

  def flatMap[B](f: A => Option[B]): Option[B] = map(f) getOrElse None
  def orElse[B>:A](ob: => Option[B]): Option[B] = map (Some(_)) getOrElse ob
  def filter(f: A => Boolean): Option[A] = flatMap (n ⇒ if (f(n)) Some(n) else None)
}

case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]

object Option {
  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)
  def variance(xs: Seq[Double]): Option[Double] = sys.error("todo")

  def map2[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    a flatMap (a ⇒ b map (b ⇒ f(a, b)))

  def sequence[A](a: List[Option[A]]): Option[List[A]] = traverse(a)(x ⇒ x)

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] =
    a.foldRight[Option[List[B]]](Some(Nil))((x,acc) ⇒ map2(f(x), acc)(_ :: _))
}